<?php
/*
*PHP Linechart
*Author MrDarkhood
*https://gitlab.com/MrDarkHooD/php-graphs/
*GNU General Public License v3.0
*Give it array with number values, it displays chart
*and percent of that value
*/

//settings
$array = [100,120,130,128,149,150,160,240,129,231,258,389]; //stuff you want to show
$hc = 5; //Horizontal rows
$iw = 855; //Image width
$ih = 315; //image height
$fs = 5; //font size 1-5
$fs2 = 1; //font size to % numbers 1-5
$st = 1; //startnumber
$percent = true; //show percent near balls. 'false' for value
$max = 0; //max number on left. 0 for default, 100 for percent

header("Content-Type: image/png");
$im = @imagecreate($iw, $ih)
	or die("Cannot Initialize new GD image stream");

$bgcolor	= imagecolorallocate($im, 255, 255, 255);
$textcolor	= imagecolorallocate($im, 0, 0, 0);
$ballcolor	= imagecolorallocate($im, 0,0,255);
$linecolor	= imagecolorallocate($im, 128,128,128);
$linecolor2	= imagecolorallocate($im, 255, 0, 0);

//Count numbers
ksort($array);
$array = array_reverse($array);
$vc = count($array);
$nav = ($iw/100)*10;
$con = ($iw/100)*90;
$bot = ($ih/100)*10;
$top = ($ih/100)*85;
$biggest = max($array);
$number = ($ih*0.27619047619)/$hc;
$max = ($max) ? $max : max($array);

//Horizontal lines and numbers
for($i=$hc,$a=0;($i)>-1;$i--,$a++) {
	$text = round($max/($hc)*$i);
	if($max == 100) $text.='%';
	imagestring($im,$fs, 5, (($a*$number)*3)+5, $text, $textcolor);
	imageline($im,0,(($a*$number)*3)+20,$iw,(($a*$number)*3)+20,$linecolor);
}

//Draw statics
for($i=0,$width=$iw,$a=$vc-1;$i<$vc;$i++,$width-=($con-30)/($vc-1),$a--) {
	$math = ($top/$biggest)*$array[$i];
	$w = $width-((strlen($array[$i])-1)*3)-10;
	$h = (($ih-$bot)-$math);
	if(!isset($last)) $last = [$w,$h];

	imagefilledarc($im, $w, $h, 10, 10, 10, 10, $ballcolor, IMG_ARC_PIE);
	imageline($im,$last[0],$last[1],$w, $h, $linecolor2);
	$text = (!$percent) ? $array[$i] : round($array[$i]/array_sum($array)*100).'%';
	imagestring($im,$fs2,$w-20, $h-10, $text, $textcolor);
	imagestring($im,$fs,$w-10, $top+20,$a+$st,$textcolor);
	$last = [$w,$h];
}

imagepng($im);
imagedestroy($im);