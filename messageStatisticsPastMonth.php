<?php
require_once("../../src/settings.php");
require_once("../../src/pdo/PDO.class.php");

$DB = new Db(
	DBHost,
	DBPort,
	DBName,
	DBUser,
	DBPassword,
	DBEngine
);

$query = "
SELECT COUNT(id) as count,
	   TO_CHAR(TO_TIMESTAMP(unixtime)::TIMESTAMPTZ AT TIME ZONE 'UTC', 'YYYYMMDD') as date 
FROM
	board_messages 
GROUP BY
	date 
ORDER BY
	date DESC 
LIMIT
	30";

$database = $DB->query(
	$query,
	array(),
	PDO::FETCH_ASSOC
);

$indexedDatabase = array();
foreach ( $database as $row )
{
    $indexedDatabase[ $row['date'] ] = $row['count'];
}

header( "Content-Type: image/png" );

$im = @imagecreate(
	855,
	315
) or die("Cannot Initialize new GD image stream");

$bgcolor    = imagecolorallocate( $im, 25,  25,  25  );
$textcolor  = imagecolorallocate( $im, 238, 238, 238 );
$barcolor   = imagecolorallocate( $im, 23,  164, 92  );
$linecolor  = imagecolorallocate( $im, 48,  48,  48  );

// Calculate the maximum count for the bars
$biggest = max( $indexedDatabase );
if ( $biggest % 50 )
{
    $biggest = $biggest - ($biggest % 50) + 50;
}

// Draw horizontal lines and count numbers for bars
for (
	$hline = 10;
	$hline >= 0;
	$hline--
)
{
	$magic =  ( ( 10 - $hline ) * 30 ) + 20;
	
    imagestring(
		$im,
		30,
		5,
		$magic - 50,
		round( $biggest / 10 * $hline ),
		$textcolor
	);
	
    imageline(
		$im,
		0,
		$magic,
		880,
		$magic,
		$linecolor
	);
}

// Draw vertical line
imageline(
	$im,
	100,
	0,
	100,
	880,
	$linecolor
);

$numberLocation = 105;
foreach ( array_reverse($indexedDatabase, true) as $date => $postcount )
{

	$dayNumber = date('d', strtotime($date));

	// Write day of the month (int)
	imagestring(
		$im,
		30,
		$numberLocation,
		295,
		$dayNumber,
		$textcolor
	);
	
	// Draw bar
	$math = (290 / $biggest) * $postcount;
	$barTop = 289 - $math;
    imagefilledrectangle(
		$im,
		$numberLocation + 5,
		289,
		$numberLocation + 15,
		$barTop,
		$barcolor
	);
	
	// Draw post count on top of bar
	imagestring(
		$im,
		1,
		$numberLocation + 10 - ( ( strlen( $postcount ) - 1) * 3),
		$barTop - 10,
		$postcount,
		$textcolor
	);

	$numberLocation += 25;
}

// Draw average count
imagestring(
	$im,
	5,
	760,
	35,
	'Average ' . round( array_sum( $indexedDatabase ) / 30 ),
	$textcolor
);

// Draw timestamp
imagestring(
	$im,
	5,
	760,
	5,
	date('d.m.Y'),
	$textcolor
);

imagepng($im);
imagedestroy($im);
?>
