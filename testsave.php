<?php
/*
*PHP Linechart
*Author MrDarkhood
*https://gitlab.com/MrDarkHooD/php-graphs/
*GNU General Public License v3.0
*Give it array with number values, it displays chart and percent of that value
*/

//settings
$array = explode(',',$_GET['nums']); //stuff you want to show
$hc = (!isset($_GET['hc'])) ? 5 : $_GET['hc']; //Horizontal rows
$iw = (!isset($_GET['w'])) ? 855 : $_GET['w']; //Image width
$ih =  (!isset($_GET['h'])) ? 315 : $_GET['h']; //image height
$fs = 5; //font size 1-5
$fs2 = 1; //font size to % numbers 1-5
$st = (!isset($_GET['st'])) ? 0 : $_GET['st']; //startnumber
$percent = (!isset($_GET['pc'])) ? true : $_GET['pc']; //show percent near balls. 'false' for value
$max = (!isset($_GET['max'])) ? 0 : $_GET['max']; //max number on left. 0 for default, 100 for percent

$bgcolor	= (!isset($_GET['bgcolor'])) ? [255,255,255] : explode(',',$_GET['bgcolor']);
$textcolor	= (!isset($_GET['textcolor'])) ? [0, 0, 0] : explode(',',$_GET['textcolor']);
$ballcolor	= (!isset($_GET['ballcolor'])) ? [0,0,255] : explode(',',$_GET['ballcolor']);
$linecolor	= (!isset($_GET['linecolor'])) ? [128,128,128] : explode(',',$_GET['linecolor']);
$linecolor2	= (!isset($_GET['linecolor2'])) ? [255, 0, 0] : explode(',',$_GET['linecolor2']);

header("Content-Type: image/png");
$im = @imagecreate($iw, $ih)
	or die("Cannot Initialize new GD image stream");

$bgcolor	= imagecolorallocate($im, $bgcolor[0], $bgcolor[1], $bgcolor[2]);
$textcolor	= imagecolorallocate($im, $textcolor[0], $textcolor[1], $textcolor[2]);
$ballcolor	= imagecolorallocate($im, $ballcolor[0], $ballcolor[1], $ballcolor[2]);
$linecolor	= imagecolorallocate($im, $linecolor[0], $linecolor[1], $linecolor[2]);
$linecolor2	= imagecolorallocate($im, $linecolor2[0], $linecolor2[1], $linecolor2[2]);

//Count numbers
ksort($array);
$array = array_reverse($array);
$vc = count($array);
$nav = ($iw/100)*10;
$con = ($iw/100)*90;
$bot = ($ih/100)*10;
$top = ($ih/100)*85;
$biggest = max($array);
$number = ($ih*0.27619047619)/$hc;
$max = ($max) ? $max : max($array);

//Horizontal lines and numbers
for($i=$hc,$a=0;($i)>-1;$i--,$a++) {
	$text = round($max/($hc)*$i);
	if($max == 100) $text.='%';
	imagestring($im,$fs, 5, (($a*$number)*3)+5, $text, $textcolor);
	imageline($im,0,(($a*$number)*3)+20,$iw,(($a*$number)*3)+20,$linecolor);
}

//Draw statics
for($i=0,$width=$iw,$a=$vc-1;$i<$vc;$i++,$width-=($con-30)/($vc-1),$a--) {
	$math = ($top/$biggest)*$array[$i];
	$w = $width-((strlen($array[$i])-1)*3)-10;
	$h = (($ih-$bot)-$math);
	if(!isset($last)) $last = [$w,$h];

	imagefilledarc($im, $w, $h, 10, 10, 10, 10, $ballcolor, IMG_ARC_PIE);
	imageline($im,$last[0],$last[1],$w, $h, $linecolor2);
	$text = (!$percent) ? $array[$i] : round($array[$i]/array_sum($array)*100).'%';
	imagestring($im,$fs2,$w-20, $h-10, $text, $textcolor);
	imagestring($im,$fs,$w-10, $top+20,$a+$st,$textcolor);
	$last = [$w,$h];
}

imagepng($im);
imagedestroy($im);